package ftn.uns.ac.rs.Web.shop.Service;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArtiklService {

    Artikl create(Artikl artikl) throws Exception;

    List<Artikl> findAll(String kategorija);

    List<Artikl> findAll();

    List<Artikl> findAllByKorpaSelektovanih(Korpa korpa);

    List<Artikl> findAllByKorpaOmiljenih(Korpa korpa);

    Artikl findOne(Long id);

    void save(Artikl artikl);

    void deleteOne(Long id);

    List<Artikl> rastuciSort();

    List<Artikl> opadajuciSort();
}
