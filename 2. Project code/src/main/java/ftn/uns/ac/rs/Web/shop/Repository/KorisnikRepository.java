package ftn.uns.ac.rs.Web.shop.Repository;

import ftn.uns.ac.rs.Web.shop.Entity.Korisnik;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

    Korisnik findByEmailAndLozinka(String email, String lozinka);

    List<Korisnik> findAllByUloga(String uloga);

}
