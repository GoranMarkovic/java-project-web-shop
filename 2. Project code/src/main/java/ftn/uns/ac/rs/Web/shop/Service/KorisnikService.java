package ftn.uns.ac.rs.Web.shop.Service;

import ftn.uns.ac.rs.Web.shop.Entity.Korisnik;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KorisnikService {

    Korisnik create(Korisnik korisnik) throws Exception;

    List<Korisnik> findAll();

    Korisnik findOne(Long id);

    Korisnik findOne(String email, String lozinka);

    void save(Korisnik korisnik);

    List<Korisnik> findAll(String uloga);

    void deleteOne(Long id);
}
