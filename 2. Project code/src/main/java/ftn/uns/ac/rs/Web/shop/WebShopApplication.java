package ftn.uns.ac.rs.Web.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

@SpringBootApplication
public class WebShopApplication {

	public static void main(String[] args) {

		SpringApplication.run(WebShopApplication.class, args);

	}

}
