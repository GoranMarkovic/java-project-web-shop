package ftn.uns.ac.rs.Web.shop.Service.Impl;

import ftn.uns.ac.rs.Web.shop.Entity.Korisnik;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import ftn.uns.ac.rs.Web.shop.Repository.KorisnikRepository;
import ftn.uns.ac.rs.Web.shop.Service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KorisnikServiceImpl implements KorisnikService {

    @Autowired
    private KorisnikRepository korisnikRepository;

    @Override
    public void save(Korisnik korisnik){
        korisnikRepository.save(korisnik);
    }

    @Override
    public Korisnik create(Korisnik korisnik) throws Exception {
        if (korisnik.getId() != null) {
            throw new Exception("ID must be null!");
        }
        Korisnik noviKorisnik = this.korisnikRepository.save(korisnik);
        return noviKorisnik;
    }

    @Override
    public Korisnik findOne(Long id) {
        Korisnik korisnik = this.korisnikRepository.getOne(id);
        return korisnik;
    }

    @Override
    public List<Korisnik> findAll() {
        List<Korisnik> korisnici = this.korisnikRepository.findAll();
        return korisnici;
    }

    @Override
    public List<Korisnik> findAll(String uloga){
        List<Korisnik> korisnici = this.korisnikRepository.findAllByUloga(uloga);
        return korisnici;
    }

    @Override
    public Korisnik findOne(String email, String lozinka){
        Korisnik korisnik = korisnikRepository.findByEmailAndLozinka(email, lozinka);
        return korisnik;
    }

    @Override
    public void deleteOne(Long id){

        this.korisnikRepository.deleteById(id);
    }
}
