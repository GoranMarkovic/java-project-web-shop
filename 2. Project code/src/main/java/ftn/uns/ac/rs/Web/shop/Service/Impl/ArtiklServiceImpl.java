package ftn.uns.ac.rs.Web.shop.Service.Impl;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import ftn.uns.ac.rs.Web.shop.Repository.ArtiklRepository;
import ftn.uns.ac.rs.Web.shop.Repository.KorpaRepository;
import ftn.uns.ac.rs.Web.shop.Service.ArtiklService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtiklServiceImpl implements ArtiklService {

    @Autowired
    private ArtiklRepository artiklRepository;

    @Autowired
    private KorpaRepository korpaRepository;

    @Override
    public Artikl create(Artikl artikl) throws Exception {
        if (artikl.getId() != null) {
            throw new Exception("ID must be null!");
        }
        Artikl noviArtikl = this.artiklRepository.save(artikl);
        return noviArtikl;
    }

    @Override
    public void save(Artikl artikl){
        this.artiklRepository.save(artikl);
    }

    @Override
    public List<Artikl> findAll(String kategorija){
        List<Artikl> artikli = artiklRepository.findAllByKategorija(kategorija);
        return artikli;
    }

    @Override
    public List<Artikl> findAll(){
        List<Artikl> artikli = artiklRepository.findAll();
        return artikli;
    }

    @Override
    public List<Artikl> findAllByKorpaSelektovanih(Korpa korpa){
        List<Artikl> artikli = artiklRepository.findAllByKorpaSelektovanih(korpa);
        return artikli;
    }

    @Override
    public List<Artikl> findAllByKorpaOmiljenih(Korpa korpa){
        List<Artikl> artikli = artiklRepository.findAllByKorpaOmiljenih(korpa);
        return artikli;
    }

    @Override
    public Artikl findOne(Long id){
        Artikl artikl = artiklRepository.getOne(id);
        return artikl;
    }

    @Override
    public void deleteOne(Long id){
        this.artiklRepository.deleteById(id);
    }

    @Override
    public List<Artikl> rastuciSort(){
        List<Artikl> artikli = this.artiklRepository.findByOrderByCenaAsc();
        return artikli;
    }

    @Override
    public List<Artikl> opadajuciSort(){
        List<Artikl> artikli = this.artiklRepository.findByOrderByCenaDesc();
        return artikli;
    }
}
