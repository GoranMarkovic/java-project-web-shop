package ftn.uns.ac.rs.Web.shop.Controller;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korisnik;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import ftn.uns.ac.rs.Web.shop.Service.ArtiklService;
import ftn.uns.ac.rs.Web.shop.Service.KorisnikService;
import ftn.uns.ac.rs.Web.shop.Service.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/artikl")
public class ArtiklController {

    @Autowired
    private ArtiklService artiklService;

    @Autowired
    private KorpaService korpaService;

    @Autowired
    private KorisnikService korisnikService;

    @GetMapping("/home")
    public String home(Model model){
        List<Artikl> artikli = artiklService.findAll();
        model.addAttribute("artikli", artikli);
        model.addAttribute("naslov", "Svi artikli");
        return "homeArtikl.html";
    }

    @GetMapping("/voce")
    public String prikazVoca(Model model){
        List<Artikl> artikli = artiklService.findAll("Voce");
        model.addAttribute("artikli", artikli);
        model.addAttribute("naslov", "Voce");
        return "homeArtikl.html";
    }

    @GetMapping("/povrce")
    public String prikazPovrca(Model model){
        List<Artikl> artikli = artiklService.findAll("Povrce");
        model.addAttribute("artikli", artikli);
        model.addAttribute("naslov", "Povrce");
        return "homeArtikl.html";
    }

    @GetMapping("/artikl/{id}")
    public String artiklDetaljnije(@PathVariable("id") Long id, Model model){
        Artikl artikl = artiklService.findOne(id);
        model.addAttribute("artikl", artikl);
        String naslov = artikl.getNaziv();
        model.addAttribute("naslov", naslov);
        return "artiklDetaljnije.html";
    }

    @GetMapping("/prikazArtikalaAdmin")
    public String prikazArtikalaZaAdmina(Model model){

        model.addAttribute("listaArtikala", this.artiklService.findAll());

        return "prikazArtikalaAdmin.html";
    }

    @GetMapping("/izmenaArtiklaAdmin/{idA}")
    public String izmenaArtiklaAdminForma(Model model, @PathVariable(name="idA") Long artikl_id){

        model.addAttribute("artikl", this.artiklService.findOne(artikl_id));

        return "izmenaArtiklaAdmin.html";
    }

    @PostMapping("/izmenaArtiklaAdmin")
    public String izmenaArtiklaAdminObrada(@ModelAttribute Artikl artikl) throws Exception{

        Artikl artiklNovi = artikl;
        this.artiklService.save(artiklNovi);

        return "redirect:/artikl/prikazArtikalaAdmin";
    }

    @GetMapping("/dodavanjeArtiklaAdmin")
    public String dodavanjeArtiklaAdminForma(Model model){

        model.addAttribute("artikl", new Artikl());

        return "dodavanjeArtiklaAdmin.html";
    }

    @PostMapping("/dodavanjeArtiklaAdmin")
    public String dodavanjeArtiklaAdminObrada(@ModelAttribute Artikl artikl) throws Exception{

        this.artiklService.create(artikl);

        return "redirect:/artikl/prikazArtikalaAdmin";
    }

    @GetMapping("/brisanjeArtiklaAdmin/{idA}")
    public String brisanjeArtiklaAdmin(@PathVariable(name="idA") Long artikl_id){

        this.artiklService.deleteOne(artikl_id);

        return "redirect:/artikl/prikazArtikalaAdmin";
    }

    @PostMapping("/sortiranje")
    public String sortiranje(@RequestParam String tipSorta, Model model){

        if(tipSorta == "rastuce"){
            model.addAttribute("artikli", this.artiklService.rastuciSort());
            model.addAttribute("naslov", "Svi artikli");
        }else if(tipSorta == "opadajuce"){
            model.addAttribute("artikli", this.artiklService.opadajuciSort());
            model.addAttribute("naslov", "Svi artikli");
        }

        return "homeArtikli.html";
    }
}