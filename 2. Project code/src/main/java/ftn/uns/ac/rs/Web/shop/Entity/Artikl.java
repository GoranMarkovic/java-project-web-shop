package ftn.uns.ac.rs.Web.shop.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Artikl implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String naziv;

    @Column
    private String opis;

    @Column
    private double cena;

    @Column
    private String kolicina;

    @Column
    private String kategorija;

    @ManyToOne(cascade = CascadeType.ALL)
    private Korpa korpaSelektovanih;

    @ManyToOne(cascade = CascadeType.ALL)
    private Korpa korpaOmiljenih;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Kupljeni_artikli",
            joinColumns = @JoinColumn(name = "artikl_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "korpa_id", referencedColumnName = "id"))
    private List<Korpa> korpaKupljenih;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getKolicina() {
        return kolicina;
    }

    public void setKolicina(String kolicina) {
        this.kolicina = kolicina;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public Korpa getKorpaSelektovanih() {
        return korpaSelektovanih;
    }

    public void setKorpaSelektovanih(Korpa korpaSelektovanih) {
        this.korpaSelektovanih = korpaSelektovanih;
    }

    public Korpa getKorpaOmiljenih() {
        return korpaOmiljenih;
    }

    public void setKorpaOmiljenih(Korpa korpaOmiljenih) {
        this.korpaOmiljenih = korpaOmiljenih;
    }

    public List<Korpa> getKorpaKupljenih() {
        return korpaKupljenih;
    }

    public void setKorpaKupljenih(List<Korpa> korpaKupljenih) {
        this.korpaKupljenih = korpaKupljenih;
    }

    @Override
    public String toString() {
        return "Artikl{" +
                "id=" + id +
                ", naziv=" + naziv +
                ", opis=" + opis +
                ", cena=" + cena +
                ", kolicina=" + kolicina +
                ", kategorija=" + kategorija +
                '}';
    }

}