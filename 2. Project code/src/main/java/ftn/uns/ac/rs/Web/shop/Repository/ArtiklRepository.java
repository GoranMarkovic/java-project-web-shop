package ftn.uns.ac.rs.Web.shop.Repository;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtiklRepository extends JpaRepository<Artikl, Long> {

    List<Artikl> findAllByKategorija(String kategorija);

    List<Artikl> findAllByKorpaSelektovanih(Korpa korpa);

    List<Artikl> findAllByKorpaOmiljenih(Korpa korpa);

    List<Artikl> findByOrderByCenaAsc();

    List<Artikl> findByOrderByCenaDesc();

}
