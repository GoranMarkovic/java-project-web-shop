package ftn.uns.ac.rs.Web.shop.Entity;


import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Korisnik implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column
    private String ime;

    @NotNull
    @Column
    private String prezime;

    @NotNull
    @Column
    private String lozinka;

    @NotNull
    @Column
    private String uloga;

    @Column
    private String email;

    @NotNull
    @Column
    private Long telefon;

    @NotNull
    @Column
    private String adresa;

    @OneToOne(cascade = CascadeType.ALL)
    private Korpa korpaS; // korpa selektovanih artikala

    @OneToOne(cascade = CascadeType.ALL)
    private Korpa korpaO; //korpa omiljenih artikala

    @OneToMany(mappedBy = "korisnik_K", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Korpa> korpaK = new ArrayList<>(); //korpe kupljenih artikala


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public String getUloga() {
        return uloga;
    }

    public void setUloga(String uloga) {
        this.uloga = uloga;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTelefon() {
        return telefon;
    }

    public void setTelefon(Long telefon) {
        this.telefon = telefon;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public Korpa getKorpaS() {
        return korpaS;
    }

    public void setKorpaS(Korpa korpaS) {
        this.korpaS = korpaS;
    }

    public Korpa getKorpaO() {
        return korpaO;
    }

    public void setKorpaO(Korpa korpaO) {
        this.korpaO = korpaO;
    }

    public List<Korpa> getKorpaK() {
        return korpaK;
    }

    public void setKorpaK(List<Korpa> korpaK) {
        this.korpaK = korpaK;
    }

    @Override
    public String toString() {
        return "Korisnik{" +
                "id=" + id +
                ", ime=" + ime +
                ", prezime=" + prezime +
                ", lozinka=" + lozinka +
                ", uloga=" + uloga +
                ", email=" + email +
                ", telefon=" + telefon +
                '}';
    }

}