package ftn.uns.ac.rs.Web.shop.Controller;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korisnik;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import ftn.uns.ac.rs.Web.shop.Repository.KorisnikRepository;
import ftn.uns.ac.rs.Web.shop.Service.ArtiklService;
import ftn.uns.ac.rs.Web.shop.Service.KorisnikService;
import ftn.uns.ac.rs.Web.shop.Service.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/korpa")
public class KorpaController {

    @Autowired
    private KorpaService korpaService;

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private ArtiklService artiklService;

    @Autowired
    private KorisnikRepository korisnikRepository;

    @GetMapping("kreiranjeKorpi/{id}")
    public String kreiranjeKorpi(@PathVariable(name="id") Long korisnik_id, Model model) throws Exception{
        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);

        Korpa korpaS = new Korpa();
        korpaS = this.korpaService.create(korpaS);
        korpaS.setKupac(korisnik.getIme() + ' ' + korisnik.getPrezime());
        korisnik.setKorpaS(korpaS);

        Korpa korpaO = new Korpa();
        korpaO = this.korpaService.create(korpaO);
        korpaO.setKupac(korisnik.getIme() + ' ' + korisnik.getPrezime());
        korisnik.setKorpaO(korpaO);

        this.korisnikService.save(korisnik);

        model.addAttribute("korisnik_id", korisnik_id);

        return "korpeKreirane.html";
    }

    @GetMapping("/kupacPocetna/{id}")
    public String kupacPocetna(@PathVariable("id") Long id, Model model) throws Exception{
        Korisnik korisnik = this.korisnikService.findOne(id);

        model.addAttribute("listaArtikala", this.artiklService.findAll());
        model.addAttribute("korisnik_id",id);
        model.addAttribute("naslov", korisnik.getIme() + ' ' + korisnik.getPrezime());

        return "kupacPocetna.html";
    }


    @GetMapping("/korpaSelektovanih/{idK}/{idA}")
    public String korpaSelektovanih(@PathVariable(name="idK") Long korisnik_id, @PathVariable(name="idA") Long artikl_id, Model model)
    {
        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa korpaSelektovanih = korisnik.getKorpaS();
        Artikl artikl = this.artiklService.findOne(artikl_id);

        artikl.setKorpaSelektovanih(korpaSelektovanih);

        this.korpaService.save(korpaSelektovanih);

        model.addAttribute("selektovaniArtikli", this.artiklService.findAllByKorpaSelektovanih(korpaSelektovanih));
        model.addAttribute("korisnik_id", korisnik_id);

        return "artikli_u_korpiS.html";
    }


    @GetMapping("/korpaOmiljenih/{idK}/{idA}")
    public String korpaOmiljenih(@PathVariable(name="idK") Long korisnik_id, @PathVariable(name="idA") Long artikl_id, Model model)
    {

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa korpaOmiljenih = korisnik.getKorpaO();
        Artikl artikl = this.artiklService.findOne(artikl_id);

        artikl.setKorpaOmiljenih(korpaOmiljenih);

        this.korpaService.save(korpaOmiljenih);

        model.addAttribute("omiljeniArtikli", this.artiklService.findAllByKorpaOmiljenih(korpaOmiljenih));
        model.addAttribute("korisnik_id", korisnik_id);

        return "artikli_u_korpiO.html";
    }




    @GetMapping("/izbaciIzSelektovanih/{idK}/{idA}")
    public String izbaciIzSelektovanih(@PathVariable(name="idK") Long korisnik_id, @PathVariable(name="idA") Long artikl_id, Model model)
    {
        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa korpa = korisnik.getKorpaS();
        Artikl artikl = this.artiklService.findOne(artikl_id);
        artikl.setKorpaSelektovanih(null);

        this.artiklService.save(artikl);

        model.addAttribute("selektovaniArtikli", this.artiklService.findAllByKorpaSelektovanih(korpa));
        model.addAttribute("korisnik_id", korisnik_id);

        return "artikli_u_korpiS.html";
    }


    @GetMapping("/izbaciIzOmiljenih/{idK}/{idA}")
    public String izbaciIzOmiljenih(@PathVariable(name="idK") Long korisnik_id, @PathVariable(name="idA") Long artikl_id, Model model)
    {

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa korpa = korisnik.getKorpaO();
        Artikl artikl = this.artiklService.findOne(artikl_id);
        artikl.setKorpaOmiljenih(null);

        this.artiklService.save(artikl);

        model.addAttribute("omiljeniArtikli", this.artiklService.findAllByKorpaOmiljenih(korpa));
        model.addAttribute("korisnik_id", korisnik_id);

        return "artikli_u_korpiO.html";
    }

    @GetMapping("/potvrdiKupovinu/{idK}")
    public String potvrdiKupovinu(@PathVariable(name="idK") Long korisnik_id, Model model) throws Exception {

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa korpa = korisnik.getKorpaS();

        Korpa kupljeno = new Korpa();
        kupljeno = this.korpaService.create(kupljeno);
        kupljeno.setKorisnik_K(korisnik);
        kupljeno.setKupac(korisnik.getIme() + ' ' + korisnik.getPrezime());
        kupljeno.setStatus("Kupljeno");

        kupljeno.setDatum("9.9.2019");

        List<Artikl> artikli_u_korpiS = korpa.getArtikli_u_korpiSelektovanih();
        for(Artikl artikl : artikli_u_korpiS){
            artikl.setKorpaSelektovanih(null);
            artikl.getKorpaKupljenih().add(kupljeno);
        }


        this.korpaService.save(kupljeno);
        this.korpaService.save(korpa);



        model.addAttribute("korisnik_id", korisnik_id);

        return "primljenaPorudzbina.html";

    }


    @GetMapping("/istorijaKupovina/{idK}")
    public String istorijaKupovina(@PathVariable(name="idK") Long korisnik_id, Model model){

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        List<Korpa> korpe = korisnik.getKorpaK();

        model.addAttribute("korpe", korpe);
        model.addAttribute("korisnik_id", korisnik_id);

        return "istorijaKupovina.html";
    }

    @GetMapping("/omiljeniArtikli/{idK}")
    public String omiljeniArtikli(@PathVariable(name="idK") Long korisnik_id, Model model){

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa korpaO = korisnik.getKorpaO();

        model.addAttribute("omiljeniArtikli", this.artiklService.findAllByKorpaOmiljenih(korpaO));
        model.addAttribute("korisnik_id", korisnik_id);

        return "omiljeniArtikli.html";
    }

    @GetMapping("/dostavljacPocetna/{idK}")
    public String dostavljacPocetna(Model model, @PathVariable(name="idK") Long korisnik_id){

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);

        model.addAttribute("porudzbine", this.korpaService.findAll("Kupljeno"));
        model.addAttribute("korisnik_id", korisnik_id);
        model.addAttribute("naslov", korisnik.getIme() + ' ' + korisnik.getPrezime());

        return "dostavljacPocetna.html";
    }

    @GetMapping("/pregledPorudzbina/{idK}/{idP}")
    public String pregledPorudzbina(Model model, @PathVariable(name="idK") Long korisnik_id, @PathVariable(name="idP") Long porudzbina_id){

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);
        Korpa porudzbina = this.korpaService.findOne(porudzbina_id);

        porudzbina.setStatus("Dostava u toku");
        porudzbina.setDostavljac(korisnik.getIme() + ' ' + korisnik.getPrezime());

        this.korpaService.save(porudzbina);

        model.addAttribute("porudzbine", this.korpaService.findAll("Dostava u toku"));
        model.addAttribute("korisnik_id", korisnik_id);

        return "prikazPreuzetihPorudzbina.html";
    }

    @GetMapping("/pregledPorudzbina/{idK}")
    public String pregledPorudzbinaPocetna(Model model, @PathVariable(name="idK") Long korisnik_id){

        model.addAttribute("korisnik_id", korisnik_id);
        model.addAttribute("porudzbine", this.korpaService.findAll("Dostava u toku"));

        return "prikazPreuzetihPorudzbina.html";
    }

    @GetMapping("/otkaziPorudzbinu/{idP}/{idK}")
    public String otkaziPorudzbinu(@PathVariable(name="idP") Long porudzbina_id, @PathVariable(name="idK") Long korisnik_id, Model model){

        Korpa porudzbina = this.korpaService.findOne(porudzbina_id);
        porudzbina.setStatus("Otkazano");

        this.korpaService.save(porudzbina);

        model.addAttribute("porudzbine", this.korpaService.findAll("Dostava u toku", "Dostavljeno"));
        model.addAttribute("korisnik_id", korisnik_id);
        return "prikazPreuzetihPorudzbina.html";
    }

    @GetMapping("/dostavljenaPorudzbina/{idP}/{idK}")
    public String dostavljenaPorudzbina(@PathVariable(name="idP") Long porudzbina_id, Model model, @PathVariable(name="idK") Long korisnik_id){

        Korpa porudzbina = this.korpaService.findOne(porudzbina_id);
        porudzbina.setStatus("Dostavljeno");

        this.korpaService.save(porudzbina);

        model.addAttribute("porudzbine", this.korpaService.findAll("Dostava u toku", "Dostavljeno"));
        model.addAttribute("korisnik_id", korisnik_id);

        return "prikazPreuzetihPorudzbina.html";
    }

    @GetMapping("/adminPocetna/{idK}")
    public String adminPocetna(Model model, @PathVariable(name="idK") Long korisnik_id) {

        Korisnik korisnik = this.korisnikService.findOne(korisnik_id);

        model.addAttribute("naslov", korisnik.getIme() + ' ' + korisnik.getPrezime());
        model.addAttribute("korisnik_id", korisnik_id);

        return "adminPocetna.html";
    }
}