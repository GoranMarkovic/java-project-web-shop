package ftn.uns.ac.rs.Web.shop.Controller;


import ftn.uns.ac.rs.Web.shop.Entity.Korisnik;
import ftn.uns.ac.rs.Web.shop.Service.KorisnikService;
import ftn.uns.ac.rs.Web.shop.Service.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.Date;

@Controller
@RequestMapping("/korisnik")
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;

    @Autowired
    private KorpaService korpaService;

    @GetMapping("/home")
    public String welcome(){
        return "home.html";
    }

    @GetMapping("/registration-form")
    public String displayRegistrationForm(Model model){

        model.addAttribute(new Korisnik());
        return "registrujKorisnika.html";
    }

    @PostMapping("/save-user")
    public String processRegistrationForm(@ModelAttribute Korisnik korisnik, Model model) throws Exception{

        this.korisnikService.create(korisnik);
        model.addAttribute("korisnik", korisnik);
        return "uspesnoRegistrovan.html";
    }

    @GetMapping("/login")
    public String loginForm(){

        return "formaPrijava.html";
    }


    @PostMapping("/login")
    public String processLoginForm(@RequestParam String email, @RequestParam String lozinka, Model model){

        Korisnik noviKorisnik = this.korisnikService.findOne(email, lozinka);

        model.addAttribute("prijavljeniKorisnik", noviKorisnik);
        return "korisnik.html";
    }

    @GetMapping("/korisnik-pregledProfila/{id}")
    public String pregledProfila(@PathVariable("id") Long id, Model model){

        Korisnik korisnik = korisnikService.findOne(id);
        model.addAttribute("korisnik", korisnik);
        return "korisnik-pregledProfila.html";
    }

    @GetMapping("/prikazDostavljaca")
    public String prikazDostavljaca(Model model){

        model.addAttribute("dostavljaci", this.korisnikService.findAll("Dostavljac"));

        return "prikazDostavljaca.html";
    }

    @GetMapping("/izmenaDostavljaca/{idD}")
    public String izmenaDostavljacaForma(Model model, @PathVariable(name="idD") Long dostavljac_id){

        model.addAttribute("dostavljac", this.korisnikService.findOne(dostavljac_id));

        return "izmenaDostavljaca.html";
    }

    @PostMapping("/izmenaDostavljaca")
    public String izmenaDostavljacaObrada(@ModelAttribute Korisnik dostavljac) throws Exception{

        Korisnik dostavljacNovi = dostavljac;
        this.korisnikService.save(dostavljacNovi);

        return "redirect:/korisnik/prikazDostavljaca";
    }

    @GetMapping("/dodavanjeDostavljaca")
    public String dodavanjeDostavljacaForma(Model model){

        model.addAttribute("dostavljac", new Korisnik());

        return "dodavanjeDostavljaca.html";
    }

    @PostMapping("/dodavanjeDostavljaca")
    public String dodavanjeDostavljacaObrada(@ModelAttribute Korisnik dostavljac) throws Exception{

        this.korisnikService.create(dostavljac);

        return "redirect:/korisnik/prikazDostavljaca";
    }

    @GetMapping("/brisanjeDostavljaca/{idD}")
    public String brisanjeDostavljaca(@PathVariable(name="idD") Long dostavljac_id){

        this.korisnikService.deleteOne(dostavljac_id);

        return "redirect:/korisnik/prikazDostavljaca";
    }

    @GetMapping("/odabirIzvestaja")
    public String OdabirIzvestaja(){

        return "izvestaji.html";
    }

    @GetMapping("/izvestaj")
    public String izvestaj(@RequestParam(required = false) String dan, Model model){

        model.addAttribute("korpe", this.korpaService.findAllByDatumAndStatus(dan, "Dostavljeno"));
        model.addAttribute("naslov", "Izvestaj za dan ");
        model.addAttribute("brojOtkazanih", this.korpaService.count("Otkazano"));

        return "stanjeIzvestaja.html";
    }

}



