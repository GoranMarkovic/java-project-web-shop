package ftn.uns.ac.rs.Web.shop.Repository;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import org.apache.tomcat.jni.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Repository
public interface KorpaRepository extends JpaRepository<Korpa, Long> {

    List<Korpa> findAllByStatus(String status);

    List<Korpa> findAllByStatusOrStatus(String status1, String status2);

    List<Korpa> findAllByDatumAndStatus(String datum, String status);

    List<Korpa> findAllByDatumBetweenAndStatus(Date datumStart, Date datumEnd, String status);

    int countByStatus(String status);
}
