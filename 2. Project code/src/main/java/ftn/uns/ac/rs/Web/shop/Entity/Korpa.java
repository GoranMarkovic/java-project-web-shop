package ftn.uns.ac.rs.Web.shop.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
@Entity
public class Korpa implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String datum;

    @Column
    private String kupac;

    @Column
    private String dostavljac;

    @Column
    private String status;

    @OneToOne(mappedBy = "korpaS")
    private Korisnik korisnik_S;

    @OneToOne(mappedBy = "korpaO")
    private Korisnik korisnik_O;

    @ManyToOne(fetch = FetchType.EAGER)
    private Korisnik korisnik_K;

    @OneToMany(mappedBy = "korpaSelektovanih", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Artikl> artikli_u_korpiSelektovanih;

    @OneToMany(mappedBy = "korpaOmiljenih", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Artikl> artikli_u_korpiOmiljenih;

    @ManyToMany(mappedBy = "korpaKupljenih", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Artikl> artikli_u_korpiKupljenih;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getKupac() {
        return kupac;
    }

    public void setKupac(String kupac) {
        this.kupac = kupac;
    }

    public String getDostavljac() {
        return dostavljac;
    }

    public void setDostavljac(String dostavljac) {
        this.dostavljac = dostavljac;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Korisnik getKorisnik_S() {
        return korisnik_S;
    }

    public void setKorisnik_S(Korisnik korisnik_S) {
        this.korisnik_S = korisnik_S;
    }

    public Korisnik getKorisnik_O() {
        return korisnik_O;
    }

    public void setKorisnik_O(Korisnik korisnik_O) {
        this.korisnik_O = korisnik_O;
    }

    public Korisnik getKorisnik_K() {
        return korisnik_K;
    }

    public void setKorisnik_K(Korisnik korisnik_K) {
        this.korisnik_K = korisnik_K;
    }

    public List<Artikl> getArtikli_u_korpiSelektovanih() {
        return artikli_u_korpiSelektovanih;
    }

    public void setArtikli_u_korpiSelektovanih(List<Artikl> artikli_u_korpiSelektovanih) {
        this.artikli_u_korpiSelektovanih = artikli_u_korpiSelektovanih;
    }

    public List<Artikl> getArtikli_u_korpiOmiljenih() {
        return artikli_u_korpiOmiljenih;
    }

    public void setArtikli_u_korpiOmiljenih(List<Artikl> artikli_u_korpiOmiljenih) {
        this.artikli_u_korpiOmiljenih = artikli_u_korpiOmiljenih;
    }

    public List<Artikl> getArtikli_u_korpiKupljenih() {
        return artikli_u_korpiKupljenih;
    }

    public void setArtikli_u_korpiKupljenih(List<Artikl> artikli_u_korpiKupljenih) {
        this.artikli_u_korpiKupljenih = artikli_u_korpiKupljenih;
    }


    @Override
    public String toString() {
        return "Korpa{" +
                "id=" + id +
                ", datum=" + datum +
                ", kupac='" + kupac + '\'' +
                ", dostavljac='" + dostavljac + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

