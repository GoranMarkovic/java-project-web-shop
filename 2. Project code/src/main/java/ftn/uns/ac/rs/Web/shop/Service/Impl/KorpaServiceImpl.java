package ftn.uns.ac.rs.Web.shop.Service.Impl;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import ftn.uns.ac.rs.Web.shop.Repository.KorpaRepository;
import ftn.uns.ac.rs.Web.shop.Service.KorpaService;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public class KorpaServiceImpl implements KorpaService {

    @Autowired
    private KorpaRepository korpaRepository;

    @Override
    public Korpa create(Korpa korpa) throws Exception {
        if (korpa.getId() != null) {
            throw new Exception("ID must be null!");
        }
        Korpa novaKorpa = this.korpaRepository.save(korpa);
        return novaKorpa;
    }

    @Override
    public Korpa findOne(Long id){
        Korpa novaKorpa = korpaRepository.getOne(id);
        return novaKorpa;
    }

    @Override
    public Korpa save(Korpa korpa){
        Korpa novaKorpa = this.korpaRepository.save(korpa);
        return novaKorpa;
    }

    @Override
    public List<Korpa> findAll(String status){
        List<Korpa> korpe = this.korpaRepository.findAllByStatus(status);
        return korpe;
    }

    @Override
    public List<Korpa> findAll(String status1, String status2){
        List<Korpa> korpe = this.korpaRepository.findAllByStatusOrStatus(status1, status2);
        return korpe;
    }

    @Override
    public List<Korpa> findAllByDatumAndStatus(String datum, String status){
        List<Korpa> korpe = this.korpaRepository.findAllByDatumAndStatus(datum, status);
        return korpe;
    }

    @Override
    public List<Korpa> findAll(Date datum1, Date datum2, String status){
        List<Korpa> korpe = this.korpaRepository.findAllByDatumBetweenAndStatus(datum1, datum2, status);
        return korpe;
    }

    @Override
    public int count(String status){
        int brojOtkazanih = this.korpaRepository.countByStatus(status);
        return brojOtkazanih;
    }


}
