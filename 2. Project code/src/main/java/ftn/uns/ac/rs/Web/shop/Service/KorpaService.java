package ftn.uns.ac.rs.Web.shop.Service;

import ftn.uns.ac.rs.Web.shop.Entity.Artikl;
import ftn.uns.ac.rs.Web.shop.Entity.Korpa;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
public interface KorpaService {

    Korpa create(Korpa korpa) throws Exception;

    Korpa findOne(Long id);

    Korpa save(Korpa korpa);

    List<Korpa> findAll(String status);

    List<Korpa> findAll(String status1, String status2);

    List<Korpa> findAllByDatumAndStatus(String datum, String status);

    List<Korpa> findAll(Date datum1, Date datum2, String status);

    int count(String status);
}
